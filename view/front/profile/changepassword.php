<?php
include_once('../../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\User\User;

Session::init();
if(Session::checkSession() == false){
    header("Location:".App\Helper::config('config.basePath'));
}
include_once('../include/header.php');

if(count( Auth::getUser()) > 0){
    $userId = Auth::getUser()['id']; //Auth::getUser() array return kara ar amara array index dara id apaitac
    $result = new User();
    $redults = $result->getAllUseInfo($userId);
    // $redults = $result->getAllUseInfo($_GET['id']);
}
?>



<div class="container">
    <div class="row">

    <div class="col-md-12 flash-massage">
        <?php

        if(App\Session::getflash('success')){
            echo '<div style="
    margin-left: 277PX;
    PADDING: 15PX;
    color: green;
"><button class="close" type="button"></button><p> Well Done : '.App\Session::getflash('success').'.</p></div>';
        }
        if(App\Session::getflash('error')){
            echo '<div style="
    margin-left: 277PX;
    PADDING: 15PX;
    color: orangered;
"><button class="close" type="button" ></button><p> Error : '.App\Session::getflash('error').'.</p></div>';
        }
        ?>
    </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <p class="text-center">Use the form below to change your password. Your password cannot be the same as your username.</p>
            <form method="post" id="passwordForm" action="view/front/profile/passUp.php">
                <input type="password" class="input-lg form-control" name="password" id="password" placeholder="Old Password" autocomplete="off">
                <input type="hidden" class="input-lg form-control" value="<?= $redults['id']?>" name="id" id="" placeholder="" autocomplete="off">
                <input type="password" class="input-lg form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off">
                <div class="row">
                    <div class="col-sm-6">
                        <span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> 8 Characters Long<br>
                        <span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Uppercase Letter
                    </div>
                    <div class="col-sm-6">
                        <span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Lowercase Letter<br>
                        <span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> One Number
                    </div>
                </div>
                <input type="password" class="input-lg form-control" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off">
                <div class="row">
                    <div class="col-sm-12">
                        <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
                    </div>
                </div>
                <input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
            </form>
        </div><!--/col-sm-6-->
    </div><!--/row-->
</div>

<script type="text/javascript">



    $("input[type=password]").keyup(function(){
        var ucase = new RegExp("[A-Z]+");
        var lcase = new RegExp("[a-z]+");
        var num = new RegExp("[0-9]+");

        if($("#password1").val().length >= 8){
            $("#8char").removeClass("glyphicon-remove");
            $("#8char").addClass("glyphicon-ok");
            $("#8char").css("color","#00A41E");
        }else{
            $("#8char").removeClass("glyphicon-ok");
            $("#8char").addClass("glyphicon-remove");
            $("#8char").css("color","#FF0004");
        }

        if(ucase.test($("#password1").val())){
            $("#ucase").removeClass("glyphicon-remove");
            $("#ucase").addClass("glyphicon-ok");
            $("#ucase").css("color","#00A41E");
        }else{
            $("#ucase").removeClass("glyphicon-ok");
            $("#ucase").addClass("glyphicon-remove");
            $("#ucase").css("color","#FF0004");
        }

        if(lcase.test($("#password1").val())){
            $("#lcase").removeClass("glyphicon-remove");
            $("#lcase").addClass("glyphicon-ok");
            $("#lcase").css("color","#00A41E");
        }else{
            $("#lcase").removeClass("glyphicon-ok");
            $("#lcase").addClass("glyphicon-remove");
            $("#lcase").css("color","#FF0004");
        }

        if(num.test($("#password1").val())){
            $("#num").removeClass("glyphicon-remove");
            $("#num").addClass("glyphicon-ok");
            $("#num").css("color","#00A41E");
        }else{
            $("#num").removeClass("glyphicon-ok");
            $("#num").addClass("glyphicon-remove");
            $("#num").css("color","#FF0004");
        }

        if($("#password1").val() == $("#password2").val()){
            $("#pwmatch").removeClass("glyphicon-remove");
            $("#pwmatch").addClass("glyphicon-ok");
            $("#pwmatch").css("color","#00A41E");
        }else{
            $("#pwmatch").removeClass("glyphicon-ok");
            $("#pwmatch").addClass("glyphicon-remove");
            $("#pwmatch").css("color","#FF0004");
        }
    });


</script>
<?php include_once('../include/footer.php'); ?>
