
<?php
include_once '../include/header.php';
?>
    <div class="container">
        <div class="row">
            <div class="error-template">
                <h1>Oops!</h1>
                <h2>404 Not Found</h2>
                <div class="error-details">
                    Sorry, an error has occured, Requested page not found!<br>
                    <?php// echo CHtml::encode($message); ?>
                </div>
                <div class="error-actions">
                    <a href="<?php //echo Yii::app()->request->baseUrl; ?>" class="btn btn-primary">
                        <i class="icon-home icon-white"></i> Take Me Home </a>
                    <a href="mailto:mail@mai.com" class="btn btn-default">
                        <i class="icon-envelope"></i> Contact Support </a>
                </div>
            </div>
        </div>
    </div>








<?php include_once '../include/footer.php'; ?>