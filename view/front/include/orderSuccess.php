<?php

include_once('../../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Cart\Cart;
use App\Order\Order;
Session::init();
$cartObj    = new Cart();
$orderObj   = new Order();
$totalPtice = $cartObj->getTotalPrice();
$products   = $cartObj->getCartAll();
$cpsc       = Session::get('CPSC');
$userData   = Auth::getUser();
if(isset($_GET['action']) && ($_GET['action'] == 'orserInsert')) {
    $orderData = array(
        'paymentId' => 4444564,
        'paymentAmount' => ($totalPtice + $cpsc),
        'paymentDate' => '2017-10-13 00:00:00',
        'paymentStatus' => 'Accept',
        'orderStatus' => 'Pending'
    );
    if (count($orderData) > 0) {
        $orderObj->set($userData['id'], $orderData, $products);
        $orderObj->insertOrder();

    }
}