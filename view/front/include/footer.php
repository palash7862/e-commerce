<hr />
<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="w3_footer_grids">
            <div class="col-md-3 w3_footer_grid">
                <h3>Contact</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>
                <ul class="address">
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1234 567 567</li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Information</h3>
                <ul class="info">
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="mail.html">Contact Us</a></li>
                    <li><a href="codes.html">Short Codes</a></li>
                    <li><a href="faq.html">FAQ's</a></li>
                    <li><a href="products.html">Special Products</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Category</h3>
                <ul class="info">
                    <li><a href="products.html">Mobiles</a></li>
                    <li><a href="products1.html">Laptops</a></li>
                    <li><a href="products.html">Purifiers</a></li>
                    <li><a href="products1.html">Wearables</a></li>
                    <li><a href="products2.html">Kitchen</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Profile</h3>
                <ul class="info">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="products.html">Today's Deals</a></li>
                </ul>
                <h4>Follow Us</h4>
                <div class="agileits_social_button">
                    <ul>
                        <li><a href="#" class="facebook"> </a></li>
                        <li><a href="#" class="twitter"> </a></li>
                        <li><a href="#" class="google"> </a></li>
                        <li><a href="#" class="pinterest"> </a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="footer-copy">
        <div class="footer-copy1">
            <div class="footer-copy-pos">
                <a href="#home1" class="scroll"><img src="images/arrow.png" alt=" " class="img-responsive" /></a>
            </div>
        </div>
        <div class="container">
            <p>&copy; 2017 Electronic Store. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
        </div>
    </div>
</div>
<!-- //footer -->
<script src="assets/front/js/jquery.min.js"></script>
<!-- for bootstrap working -->
<script type="text/javascript" src="assets/front/js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
<!-- cart-js -->
<script src="assets/front/js/minicart.js" type="text/javascript"></script>

<script src="assets/front/js/easyResponsiveTabs.js" type="text/javascript"></script>
<!-- pop-up-box -->
<script src="assets/front/js/jquery.magnific-popup.js" type="text/javascript"></script>

<!--//pop-up-box -->
<script src="assets/front/js/jquery.countdown.js"></script>
<script src="assets/front/js/jquery.wmuSlider.js"></script>

<script type="text/javascript" src="assets/front/js/jquery.flexisel.js"></script>


<!-- flexslider -->
<script defer src="assets/front/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="assets/front/css/flexslider.css" type="text/css" media="screen" />
<script>
    // Can also be used with $(document).ready()
    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>
<!-- flexslider -->
<!-- zooming-effect -->
<script src="assets/front/js/imagezoom.js"></script>
<!-- //zooming-effect -->

<!--quantity-->
<script>
    $('.value-plus1').on('click', function(){
        var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)+1;
        divUpd.text(newVal);
    });

    $('.value-minus1').on('click', function(){
        var divUpd = $(this).parent().find('.value1'), newVal = parseInt(divUpd.text(), 10)-1;
        if(newVal>=1) divUpd.text(newVal);
    });
</script>
<!--quantity-->


<script src="assets/front/js/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab1').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });
</script>

<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo2").flexisel({
            visibleItems:4,
            animationSpeed: 1000,
            autoPlay: true,
            autoPlaySpeed: 3000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems:2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
<script type="text/javascript" src="assets/front/js/jquery.flexisel.js"></script>






<script src="assets/front/js/script.js"></script>
<script>
    $('.example1').wmuSlider();
</script>

<script>
    w3ls.render();

    w3ls.cart.on('w3sb_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
            }
        }
    });
</script>
<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo1").flexisel({
            visibleItems: 4,
            animationSpeed: 1000,
            autoPlay: true,
            autoPlaySpeed: 3000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems:2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });
</script>

<!-- start-smooth-scrolling -->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<script>
    w3ls.render();

    w3ls.cart.on('w3sb_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
            }
        }
    });
</script>
<!-- //cart-js -->
<script>
    $('#myModal88').modal('show');
</script>
<script>
    $(document).ready(function() {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('.add-product-cart').live('click', function(event){
            event.preventDefault();
            var productId = $(this).data('id');
            console.log(productId);
            jQuery.ajax({
                url: 'view/front/include/cart.php',
                type: 'post',
                dataType:'json',
                data: {cartAction: 'addProduct',  pid: productId},
                success: function (response) {
                    if( (response.status === true)){
                        if(response.cartData != false) {
                            var cartData = response.cartData,
                                cartHtml = '';
                            Object.keys(cartData).forEach(function (index) {
                                console.log('ix - ' + index); //cartData[index].name
                                cartHtml += '<li class="sbmincart-item"  data-id="' + cartData[index].id +'">';

                                cartHtml += '<div class="sbmincart-details-image">';
                                cartHtml += '<img src="view/' + cartData[index].image + '" alt="" />';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-name">';
                                cartHtml += '<a class="sbmincart-name" href="">' + cartData[index].name + '</a>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-quantity">';
                                cartHtml += '<span>' + cartData[index].qti + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-price">';
                                cartHtml += '<span class="sbmincart-price">$' + cartData[index].price + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-subprice">';
                                cartHtml += '<span class="sbmincart-subprice">$' + cartData[index].Subtotal + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-remove">';
                                cartHtml += '<button class="cart-item-remove" type="button" >×</button>';
                                cartHtml += '</div>';

                                cartHtml += '</li>';
                            })

                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > ul').append(cartHtml);
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }else {
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }
                    }
                }
            });
        });

        $('.sbmincart-details-remove').live('click', function(event){
            event.preventDefault();
            var productId = $(this).parents('li').data('id');
            console.log(productId);
            jQuery.ajax({
                url: 'view/front/include/cart.php',
                type: 'post',
                dataType:'json',
                data: {cartAction: 'removeProduct',  pid: productId},
                success: function (response) {
                    if( (response.status === true) ){
                        if(response.cartData != false) {
                            var cartData = response.cartData,
                                cartHtml = '';
                            Object.keys(cartData).forEach(function (index) {
                                cartHtml += '<li class="sbmincart-item" data-id="' + cartData[index].id +'">';

                                cartHtml += '<div class="sbmincart-details-image">';
                                cartHtml += '<img src="view/' + cartData[index].image + '" alt="" />';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-name">';
                                cartHtml += '<a class="sbmincart-name" href="">' + cartData[index].name + '</a>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-quantity">';
                                cartHtml += '<span>' + cartData[index].qti + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-price">';
                                cartHtml += '<span class="sbmincart-price">$' + cartData[index].price + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-subprice">';
                                cartHtml += '<span class="sbmincart-subprice">$' + cartData[index].Subtotal + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-remove">';
                                cartHtml += '<button class="cart-item-remove" type="button" >×</button>';
                                cartHtml += '</div>';

                                cartHtml += '</li>';
                            })

                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > ul').append(cartHtml);
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }else {
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }
                    }
                }
            });
        });

        $('.update-cart-product').live('click', function(event){
            event.preventDefault();
            var currentEl   = $(this),
                productId   = currentEl.parents('tr').data('id'),
                productQti  = currentEl.parents('tr').find('input.input-uantity').val();

            console.log(productQti);
            jQuery.ajax({
                url: 'view/front/include/cart.php',
                type: 'post',
                dataType:'json',
                data: {
                    pid         : productId,
                    cartAction  : 'updateProduct',
                    productData : {qti: productQti}
                },
                success: function (response) {
                    if( (response.status === true)){
                        if(response.cartData != false) {
                            var cartData = response.cartData,
                                cartHtml = '';
                            Object.keys(cartData).forEach(function (index) {
                                console.log('ix - ' + index); //cartData[index].name
                                cartHtml += '<li class="sbmincart-item"  data-id="' + cartData[index].id +'">';

                                cartHtml += '<div class="sbmincart-details-image">';
                                cartHtml += '<img src="view/' + cartData[index].image + '" alt="" />';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-name">';
                                cartHtml += '<a class="sbmincart-name" href="">' + cartData[index].name + '</a>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-quantity">';
                                cartHtml += '<span>' + cartData[index].qti + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-price">';
                                cartHtml += '<span class="sbmincart-price">$' + cartData[index].price + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-subprice">';
                                cartHtml += '<span class="sbmincart-subprice">$' + cartData[index].Subtotal + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-remove">';
                                cartHtml += '<button class="cart-item-remove" type="button" >×</button>';
                                cartHtml += '</div>';

                                cartHtml += '</li>';
                            })

                            currentEl.parents('tr').find('td[data-th="Subtotal"]').text('$'+cartData[productId].Subtotal);

                            var cartTableFooter = $('table#cart > tfoot');
                            cartTableFooter.find('.Subtotal').html('$'+response.totalPrice+ '<br><hr>$' +response.CPSC);
                            cartTableFooter.find('.totalprice').text('$'+ (response.totalPrice+response.CPSC));
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > ul').append(cartHtml);
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }else {
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }
                    }
                }
            });
        });

        $('.remove-cart-product').live('click', function(event){
            event.preventDefault();
            var currentEl   = $(this),
                productId   = currentEl.parents('tr').data('id');

            console.log(productId);
            jQuery.ajax({
                url: 'view/front/include/cart.php',
                type: 'post',
                dataType:'json',
                data: {
                    pid         : productId,
                    cartAction  : 'removeProduct',
                },
                success: function (response) {

                    if( (response.status === true)){
                        if(response.cartData != false) {
                            var cartData = response.cartData,
                                cartHtml = '';

                            Object.keys(cartData).forEach(function (index) {
                                //console.log('ix - ' + index); //cartData[index].name
                                cartHtml += '<li class="sbmincart-item"  data-id="' + cartData[index].id +'">';

                                cartHtml += '<div class="sbmincart-details-image">';
                                cartHtml += '<img src="view/' + cartData[index].image + '" alt="" />';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-name">';
                                cartHtml += '<a class="sbmincart-name" href="">' + cartData[index].name + '</a>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-quantity">';
                                cartHtml += '<span>' + cartData[index].qti + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-price">';
                                cartHtml += '<span class="sbmincart-price">$' + cartData[index].price + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-subprice">';
                                cartHtml += '<span class="sbmincart-subprice">$' + cartData[index].Subtotal + '</span>';
                                cartHtml += '</div>';

                                cartHtml += '<div class="sbmincart-details-remove">';
                                cartHtml += '<button class="cart-item-remove" type="button" >×</button>';
                                cartHtml += '</div>';

                                cartHtml += '</li>';
                            })

                            currentEl.parents('tr').remove();
                            var cartTableFooter = $('table#cart > tfoot');
                            cartTableFooter.find('.Subtotal').html('$'+response.totalPrice+ '<br><hr>$' +response.CPSC);
                            cartTableFooter.find('.totalprice').text('$'+ (response.totalPrice+response.CPSC));
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > ul').append(cartHtml);
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');

                        }else {
                            $('#cart-box-area  > ul').empty();
                            $('#cart-box-area  > .sbmincart-footer > .sbmincart-subtotal').text('Subtotal: $'+response.totalPrice+' USD');
                        }
                    }
                }
            });
        });


        // ----------------- sbmincart related  --------------------
        $('#cart-box-area').fadeOut();

        $('.w3view-cart').live('click', function(event) {
            event.preventDefault();
            $('#cart-box-area').fadeIn();
        });

        $('.sbmincart-closer').live('click', function(event) {
            event.preventDefault();
            $('#cart-box-area').fadeOut();
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){

        // Step show event
        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if(stepPosition === 'first'){
                $("#prev-btn").addClass('disabled');
            }else if(stepPosition === 'final'){
                $("#next-btn").addClass('disabled');
            }else{
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
            }
        });
    });
</script>
</body>
</html>