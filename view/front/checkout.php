<?php
include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Product\Product;
use App\Cart\Cart;
use App\Order\Order;

Session::init();
$cpsc        = Session::get('CPSC');
$productObj  = new Product();
$cartObj     = new Cart();
$allProducts = $productObj->getAllProducts();
$cartData    = $cartObj->getCartAll();
$subtotal    = $cartObj->getTotalPrice();


include_once 'include/header.php';
?>

<style>
    #order-wizard {
        margin-bottom: 60px;
        margin-top: 60px;
    }
    #order-wizard ul {
        display: block;
        margin-bottom: 30px;
        overflow: hidden;
    }
    #order-wizard ul li {
        background: #ddd none repeat scroll 0 0;
        border-right: 1px solid #fff;
        float: left;
        padding: 20px 5px;
        text-align: center;
        width: 25%;
    }
    #order-wizard .address_delivery {
        border: 1px solid #ddd;
    }
    #order-wizard .address_delivery > li {
        background: transparent none repeat scroll 0 0;
        display: block;
        float: none;
        padding: 10px;
        width: 100%;
    }
    .cart-btn-footer {
        display: block;
        overflow: hidden;
        width: 100%;
    }
    .cart-btn-footer .left {
        float: left;
        text-align: left;
        width: 50%;
    }
    .cart-btn-footer .right {
        float: right;
        text-align: right;
        width: 50%;
    }
</style>
<div class="container ">

    <div id="order-wizard">
        <ul>
            <?php if($cartObj->cartItem()> 0 || Order::stap() == 3 || Order::stap() == 4){
                echo '<li style="border-bottom: 1px solid green"><a href="step-1" >CheckOut</a></li>';
            }else{
                echo '<li><a href="step-1" >CheckOut</a></li>';
            } ?>
            <?php if(Order::stap() == 3 || Order::stap() == 4){ ?>
                <li style="border-bottom: 1px solid green"><a href="#step-2">Login</a></li>
                <li style="border-bottom: 1px solid green"><a href="#step-2">Address</a></li>
            <?php }else{ ?>
                <li ><a href="#step-2">Login</a></li>
                <li><a href="#step-2">Address</a></li>
            <?php } ?>
            <li><a href="#step-2">Checkout</a></li>
        </ul>
        <div id="order-wizard-content" class="">
            <?php if(Order::stap() == 2){ ?>
            <div class="inner-order-wizard-content">
                <div class="row">
                    <div class="col-md-6">
                        <form action="view/front/register.php" method="post">
                            <h3 class="page-subheading">Already registered?</h3>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input placeholder="Name" name="name" type="text" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input placeholder="Email Address" name="email" type="email" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input placeholder="Password" name="password" type="password" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input placeholder="Confirm Password" name="confirmpassword" type="password" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input placeholder="Your Mobile" name="mobile" type="text" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <textarea placeholder="Your Address" name="address" required=""></textarea>
                            </div>
                            <div class="sign-up form-group">
                                <input type="submit" value="Create Account"/>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="view/front/login.php" method="post">
                            <h3 class="page-subheading">Already registered?</h3>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control" name="email" placeholder="Username Or Email Address" type="text" required="">
                            </div>
                            <div class="form-group">
                                <label for="email">Password</label>
                                <input class="form-control" name="password" placeholder="Password" type="password" required="">
                            </div>
                            <div class="form-group">
                                <a href="" title="Recover your forgotten password" rel="nofollow">Forgot your password?</a>
                            </div>
                            <div class="form-group sign-up">
                                <input type="submit" value="Sign in"/>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <a href="view/front/checkout.php?stap=2" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if(Order::stap() == 3){ ?>
                <div class="inner-order-wizard-content">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="address_delivery">
                                <li><h3 class="page-subheading">Your delivery address</h3></li>
                                <li>Palash khan</li>
                                <li>saner akra , Dhaka</li>
                                <li>1213 Dhaka</li>
                                <li>Bangladesh</li>
                                <li>01853730155</li>
                                <li><a class="button button-small btn btn-default" href="https://brandbanglaeshop.com/address.php" title="Update">Update <i class="icon-chevron-right right"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="address_delivery">
                                <li><h3 class="page-subheading">Your billing address</h3></li>
                                <li>Palash khan</li>
                                <li>saner akra , Dhaka</li>
                                <li>1213 Dhaka</li>
                                <li>Bangladesh</li>
                                <li>01853730155</li>
                                <li><a class="button button-small btn btn-default" href="https://brandbanglaeshop.com/address.php" title="Update">Update <i class="icon-chevron-right right"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <form action="view/front/checkout.php" method="post">
                                <input type="hidden" name="stap" value="3">
                                <a href="view/front/checkout.php?stap=3" class=""></a>
                                <div class="cart-btn-footer">
                                    <div class="left"></div>
                                    <div class="right">
                                        <button type="submit" name="processAddress" class="btn btn-success btn-lg">Checkout <i class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if( Order::stap() == 1 ){ ?>
            <div class="inner-order-wizard-content">
                <?php if($cartObj->cartItem()> 0){ ?>
                <div class="row">
                    <table id="cart" class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th style="width:50%">Product</th>
                                <th style="width:10%">Price</th>
                                <th style="width:8%">Quantity</th>
                                <th style="width:22%" class="text-center">Subtotal</th>
                                <th style="width:10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if( (count($cartData)>0) && ($cartData != false) ) {
                                foreach ($cartData as $cartProduct){
                            ?>
                            <tr data-id="<?php echo $cartProduct['id'] ?>">
                                <td data-th="Product">
                                    <div class="row">
                                        <div class="col-sm-2 hidden-xs"><img src="<?php echo 'view/'.$cartProduct['image'] ?>" alt="..." class="img-responsive"/></div>
                                        <div class="col-sm-10">
                                            <h4 class="nomargin" style="text-align: center"><?php echo $cartProduct['name'] ?></h4>
                                        </div>
                                    </div>
                                </td>
                                <td data-th="Price"><?php echo '$'.$cartProduct['price'] ?></td>
                                <td data-th="Quantity">
                                    <input type="number" min="1" class="form-control text-center input-uantity" value="<?php echo $cartProduct['qti']; ?>">
                                </td>
                                <td data-th="Subtotal" class="text-center"><?php
                                    echo '$'.$cartProduct['price'] * $cartProduct['qti'] ;

                                    ?></td>
                                <td class="actions" data-th="">
                                    <button class="btn btn-info btn-sm update-cart-product"><i class="fa fa-refresh"></i></button>
                                    <button class="btn btn-danger btn-sm remove-cart-product"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            <?php
                                    }
                                }
                            ?>
                        </tbody>
                        <tfoot style="
                    border-top: 1px solid #ddd;
                ">
                            <tr>
                                <td>   </td>
                                <td>   </td>
                                <td>   </td>
                                <td>
                                    <h5>Subtotal<br>
                                        <hr />Estimated shipping
                                    </h5>
                                    <hr />
                                    <h3>Total</h3>  <hr />
                                </td>
                                <td class="text-right">
                                    <h5>
                                        <strong class="Subtotal"><?php echo '$'.$subtotal; ?><br>
                                            <hr />$<?php echo $cpsc; ?></strong>
                                    </h5>  <hr />
                                    <h3 class="totalprice"><?php echo '$'.($subtotal + $cpsc); ?></h3>
                                    <hr />
                                </td>
                            </tr>
                            <tr class="visible-xs">
                                <td class="text-center"><strong>Total 1.9</strong></td>
                            </tr>
                            <tr>
                                <td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                <td colspan="2" class="hidden-xs"></td>
                                <td class="hidden-xs text-center"><strong></strong></td>
                                <td><a href="view/front/checkout.php?stap=2" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <?php }else{
                    echo "<h3 style='text-align: center'>Your is empty</h3>";
                } ?>
            </div>
            <?php } ?>

            <?php if( Order::stap() == 4 ){ ?>
                <div class="inner-order-wizard-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="cart" class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th style="width:50%">Product</th>
                                    <th style="width:10%">Price</th>
                                    <th style="width:8%">Quantity</th>
                                    <th style="width:22%" class="text-center">Subtotal</th>
                                    <th style="width:10%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if( (count($cartData)>0) && ($cartData != false) ) {
                                    foreach ($cartData as $cartProduct){
                                        ?>
                                        <tr data-id="<?php echo $cartProduct['id'] ?>">
                                            <td data-th="Product">
                                                <div class="row">
                                                    <div class="col-sm-2 hidden-xs"><img src="<?php echo 'view/'.$cartProduct['image'] ?>" alt="..." class="img-responsive"/></div>
                                                    <div class="col-sm-10">
                                                        <h4 class="nomargin" style="text-align: center"><?php echo $cartProduct['name'] ?></h4>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-th="Price"><?php echo '$'.$cartProduct['price'] ?></td>
                                            <td data-th="Quantity">
                                                <input type="number" min="1" class="form-control text-center input-uantity" value="<?php echo $cartProduct['qti']; ?>">
                                            </td>
                                            <td data-th="Subtotal" class="text-center"><?php
                                                echo '$'.$cartProduct['price'] * $cartProduct['qti'] ;

                                                ?></td>
                                            <td class="actions" data-th="">
                                                <button class="btn btn-info btn-sm update-cart-product"><i class="fa fa-refresh"></i></button>
                                                <button class="btn btn-danger btn-sm remove-cart-product"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                                <tfoot style="
                        border-top: 1px solid #ddd;
                    ">
                                <tr>
                                    <td>   </td>
                                    <td>   </td>
                                    <td>   </td>
                                    <td>
                                        <h5>Subtotal<br>
                                            <hr />Estimated shipping
                                        </h5>
                                        <hr />
                                        <h3>Total</h3>  <hr />
                                    </td>
                                    <td class="text-right">
                                        <h5>
                                            <strong class="Subtotal"><?php echo '$'.$subtotal; ?><br>
                                                <hr />$<?php echo $cpsc; ?></strong>
                                        </h5>  <hr />
                                        <h3 class="totalprice"><?php echo '$'.($subtotal + $cpsc); ?></h3>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="visible-xs">
                                    <td class="text-center"><strong>Total 1.9</strong></td>
                                </tr>
                                <tr>
                                    <td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                    <td colspan="2" class="hidden-xs"></td>
                                    <td class="hidden-xs text-center"><strong></strong></td>
                                    <td><a href="view/include/front/orderSuccess.php?action=orserInsert" class="btn btn-success btn-block">place order <i class="fa fa-angle-right"></i></a></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!--
                        <div class="col-md-12">
                            <div class="payment-option-wrapper">
                                <div class="how_to_pay">
                                    <h3>Choose how to pay</h3>
                                    <span>* Required fields </span>
                                </div>
                                <div class="ssl" id="ssl_div">
                                    <button class="accordion" id="btn_ssl">
                                        <input id="rad_ssl" style="margin-right: 20px;" type="radio">Pay Online
                                    </button>
                                    <div class="panel">
                                        <div id="test-payment">
                                            <p class="ssl_payment">
                                                <a href="javascript:$('#sslcommerz_form').submit();" title="Pay Online ">
                                                    Pay Online
                                                    <span class="payment-logos">
                                   <img src="http://androidpin.com/public/assets/images/payonline.jpg" alt="SSL Commerzzz">
                                </span>
                                                </a>
                                            </p>
                                            <form class="hidden" action="" method="post">
                                                <input name="version" value="3.00" type="hidden">
                                                <input id="total_amount" name="total_amount" readonly="" value="1350" type="hidden">
                                                <input name="tran_id" readonly="" value="20171013062110-252" type="hidden">
                                                <input name="store_id" readonly="" value="test_thyrocarebd" type="hidden">
                                                <input name="currency" readonly="" value="BDT" type="hidden">
                                                <input name="cus_name" readonly="" value="dcvbcvbcvb" type="hidden">
                                                <input name="cus_email" readonly="" value="cvxvx@gmail.com" type="hidden">
                                                <input name="cus_phone" readonly="" value="51248623132" type="hidden">
                                                <input name="cus_add1" readonly="" value="cbvccvbc" type="hidden">

                                                <input name="success_url" readonly="" value="http://androidpin.com/payment/sslvalidation" type="hidden">
                                                <input name="fail_url" readonly="" value="http://androidpin.com/payment/sslfaild" type="hidden">
                                                <input name="cancel_url" readonly="" value="http://androidpin.com/payment/sslcancel" type="hidden">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="cod" id="cod_div" style="">
                                        <button class="accordion" id="btn_cod">
                                            <input id="rad_cod" style="margin-right: 20px;" type="radio">Cash On Delivery
                                        </button>
                                        <div class="panel" id="view_cod" style="display: block;">
                                            <div id="test-payment">
                                                <p class="ssl_payment">
                                                    <a href="view/front/include/orderSuccess.php" title="Cash On Delivery">
                                                        Cash On Delivery
                                                        <span class="payment-logos">
                                <img src="http://androidpin.com/public/assets/images/cash.png" alt="Cash On Delivery">
                                </span>
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<?php
include_once 'include/footer.php';
?>
