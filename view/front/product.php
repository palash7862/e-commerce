<?php

include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Product\Product;
use App\Category\Category;
use App\Cart\Cart;

Session::init();
$catObj      = new Category();
$productObj  = new Product();
$cartObj     = new Cart();
$cartData    = $cartObj->getCartAll();
$subtotal    = $cartObj->getTotalPrice();
$parentsCats = $catObj->getAllParentsCategory();
if(isset($_GET['catid'])){
    $allProducts = $productObj->getProductsByCatId($_GET['catid']); //
}else {
    $allProducts = $productObj->getAllProducts(); //
}

//echo "<pre>";
//print_r( $CartObj->getCartAll() );
//echo "</pre>";

?>
<?php include_once('include/header.php'); ?>
<div class="banner banner1">
    <div class="container">
        <h2>Great Offers on <span>Mobiles</span> Flat <i>35% Discount</i></h2>
    </div>
</div>

<div class="breadcrumb_dress">
    <div class="container">
        <ul>
            <li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a> <i>/</i></li>
            <li>Products</li>
        </ul>
    </div>
</div>

<div class="mobiles">
    <div class="container">
        <div class="w3ls_mobiles_grids">
            <div class="col-md-4 w3ls_mobiles_grid_left">
                <div class="w3ls_mobiles_grid_left_grid">
                    <h3>Categories</h3>
                    <div class="w3ls_mobiles_grid_left_grid_sub">
                        <ul class="panel_bottom">
                            <?php
                                if( (count($parentsCats)> 0 ) ){
                                    foreach ($parentsCats as $cat){
                            ?>
                            <li><a href="view/front/product.php?catid=<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></a></li>
                             <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="w3ls_mobiles_grid_left_grid">
                    <h3>Price</h3>
                    <div class="w3ls_mobiles_grid_left_grid_sub">
                        <div class="ecommerce_color ecommerce_size">
                            <ul>
                                <li><a href="#">Below $ 100</a></li>
                                <li><a href="#">$ 100-500</a></li>
                                <li><a href="#">$ 1k-10k</a></li>
                                <li><a href="#">$ 10k-20k</a></li>
                                <li><a href="#">$ Above 20k</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 w3ls_mobiles_grid_right">
                <div class="col-md-6 w3ls_mobiles_grid_right_left">
                    <div class="w3ls_mobiles_grid_right_grid1">
                        <img src="assets/front/images/46.jpg" alt=" " class="img-responsive">
                        <div class="w3ls_mobiles_grid_right_grid1_pos1">
                            <h3>Smart Phones<span>Up To</span> 15% Discount</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 w3ls_mobiles_grid_right_left">
                    <div class="w3ls_mobiles_grid_right_grid1">
                        <img src="assets/front/images/47.jpg" alt=" " class="img-responsive">
                        <div class="w3ls_mobiles_grid_right_grid1_pos">
                            <h3>Top 10 Latest<span>Mobile </span>&amp; Accessories</h3>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>

                <div class="w3ls_mobiles_grid_right_grid3" style="padding-top: 20px;">

                    <?php
                        if( ($allProducts != null) && (count($allProducts)>0) ){
                            foreach ($allProducts as $product){
                    ?>
                    <div class="col-md-6 agileinfo_new_products_grid agileinfo_new_products_grid_mobiles" style="margin-bottom: 25px;">
                        <div class="agile_ecommerce_tab_left mobiles_grid">
                            <div class="hs-wrapper hs-wrapper2">
                                <?php
                                $image          = json_decode( explode('; ', $product['images'] )[0] );
                                    echo '<img style="animation:0s ease 0s normal none 1 running none" src="view/'.$image->productImg.'" alt=" " class="img-responsive">';
                                ?>
                                <div class="w3_hs_bottom w3_hs_bottom_sub1">
                                    <ul>
                                        <li>
                                            <a href="view/front/productSingle.php?id=<?php echo $product['id']; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><a href="single.html"><?php echo $product['name']; ?></a></h5>
                            <div class="simpleCart_shelfItem">
                                <p><i class="item_price">$<?php echo $product['price']; ?></i></p>
                                <button type="submit" class="w3ls-cart add-product-cart" data-id="<?php echo $product['id']; ?>">Add to cart</button>
                            </div>
                            <div class="mobiles_grid_pos">
                                <h6>New</h6>
                            </div>
                        </div>
                    </div>
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="newsletter">
    <div class="container">
        <div class="col-md-6 w3agile_newsletter_left">
            <h3>Newsletter</h3>
            <p>Excepteur sint occaecat cupidatat non proident sunt.</p>
        </div>
        <div class="col-md-6 w3agile_newsletter_right">
            <form action="#" method="post">
                <input name="Email" placeholder="Email" required="" type="email">
                <input value="" type="submit">
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<?php include_once('include/footer.php'); ?>
