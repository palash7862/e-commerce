s
<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>About</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam dolore.
                </p>
            </div>
            <div class="col-md-3 col-sm-6 col-xs12 footer-block">
                <h2>Subscribe Email</h2>
                <div class="subscribe-form">
                    <form action="javascript:;">
                        <div class="input-group">
                            <input type="text" placeholder="mail@email.com" class="form-control">
                            <span class="input-group-btn">
							<button class="btn" type="submit">Submit</button>
							</span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>Follow Us On</h2>
                <ul class="social-icons">
                    <li>
                        <a href="javascript:;" data-original-title="rss" class="rss"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="youtube" class="youtube"></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
                <h2>Contacts</h2>
                <address class="margin-bottom-40">
                    Phone: 800 123 3456<br>
                    Email: <a href="mailto:info@metronic.com">info@metronic.com</a>
                </address>
            </div>
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="container">
        2014 &copy; Metronic. All Rights Reserved.
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/admin/global/plugins/respond.min.js"></script>
<script src="assets/admin/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/admin/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- THIS IS DROPZONE JS FILE UPLOAD -->
<script src="assets/admin/global/plugins/dropzone/dropzone.js"></script>

<script src="assets/admin/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="assets/admin/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>


<script type="text/javascript" src="assets/admin/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/admin/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/admin/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<!--<script type="text/javascript" src="assets/admin/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script> -->
<script type="text/javascript" src="assets/admin/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="assets/admin/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="assets/admin/admin/pages/scripts/form-dropzone.js"></script>
<script src="assets/admin/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="assets/admin/admin/pages/scripts/table-advanced.js"></script>
<script>

    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        FormDropzone.init();
        TableAdvanced.init();
        //UIExtendedModals.init();
        setTimeout(function(){
            jQuery(".flash-massage").fadeOut(500).remove();
        }, 3000);

        var mediaModel = $('#media-manager');
        $('#media-open').live('click', function(event){
            event.preventDefault();
            // create the backdrop and wait for next modal to be triggered
            //$('body').modalmanager('loading');

            jQuery.ajax({
                url: 'view/admin/product/ajaxMedia.php',
                type: 'post',
                dataType:'json',
                //data: {mediaId: mediaId},
                success: function (response) {
                    if(response.status === true){
                        var mediaHtml = '<div class="media-items">';
                        var mediaData = response.mediaData;
                        mediaData.forEach(function (item, index) {
                            mediaHtml += '<div class="single-item" data-id="'+item.id+'" data-name="'+item.name+'">'
                                var location = JSON.parse(item.location);
                                mediaHtml += '<img src="view/'+location.small+'">'
                            mediaHtml += '</div>'
                        });
                        mediaHtml += '</div>';
                        mediaHtml += '</div>';
                        mediaModel.find('.modal-body').empty();
                        mediaModel.find('.modal-body').append(mediaHtml);
                    }
                }
            });

            mediaModel.fadeIn();
        });

        var mediaBody = mediaModel.find('.modal-body > .media-items');
        mediaBody.find('.single-item').live('click', function (event) {
            $(this).addClass('active').siblings().removeClass('active');
        });


        mediaModel.find('.media-insert').live('click', function (event) {
                var tableBody = $(this).parent('.modal-footer').prev('.modal-body');
                    mediaId   = tableBody.find('.single-item.active').data('id'),
                    mediaName   = tableBody.find('.single-item.active').data('name'),
                    mediaSrc  = tableBody.find('.single-item.active').children('img').attr('src'),
                    medaiTableHtml = '';
                    console.log(mediaSrc);
                medaiTableHtml += '<tr>';
                medaiTableHtml += '<td>';
                medaiTableHtml += '<img class="img-responsive" src="'+mediaSrc+'" alt="">';
                medaiTableHtml += '<input type="hidden" name="productImgs[]" value="'+mediaId+'">';
                medaiTableHtml += '</td>';

                medaiTableHtml += '<td>';
                medaiTableHtml += mediaName;
                medaiTableHtml += '</td>';

                medaiTableHtml += '<td>';
                medaiTableHtml += '<label>';
                //medaiTableHtml += '<div class="radio"><span>';
                medaiTableHtml += '<input name="productImgPrimary" value="'+mediaId+'" type="radio">';
                //medaiTableHtml += '</div></span>';
                medaiTableHtml += '</label>';
                medaiTableHtml += '</td>';

                medaiTableHtml += '<td>';
                medaiTableHtml += '<a class="btn default btn-sm productImg-remove" href=""><i class="fa fa-times"></i> Remove </a>';
                medaiTableHtml += '</td>';
                medaiTableHtml += '</tr>';
                $('.product-images').find('tbody').append(medaiTableHtml);
                mediaModel.fadeOut();

            });
        $('.media-manager-close').live('click', function (event) {
            event.preventDefault();
            mediaModel.fadeOut();
        });
        $('.productImg-remove').live('click', function (event) {
            event.preventDefault();
            $(this).parents('tr').remove();
        });

    });
</script>





<script>


    $(document).ready(function() {
        var panels = $('.user-infos');
        var panelsButton = $('.dropdown-user');
        panels.hide();

        //Click dropdown
        panelsButton.click(function() {
            //get data-for attribute
            var dataFor = $(this).attr('data-for');
            var idFor = $(dataFor);

            //current button
            var currentButton = $(this);
            idFor.slideToggle(400, function() {
                //Completed slidetoggle
                if(idFor.is(':visible'))
                {
                    currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
                }
                else
                {
                    currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
                }
            })
        });


        $('[data-toggle="tooltip"]').tooltip();


    });

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<?php
App\Session::keyUnSet('flash');
?>