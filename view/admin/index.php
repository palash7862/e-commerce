<?php
include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Product\Product;
use App\Order\Order;


Session::init();
$productObj = new Product();

$recentSellsProducts = $productObj->getProductWithSallDetials(5);
//var_dump($recentSellsProducts);
?>
<?php

    if ( Auth::checkRule('Admin') === true || Auth::checkRule('Editor') === true ) {

?>
<?php include_once('include/header.php'); ?>
        
<?php include_once('layouts/dashboard.php'); ?>

<?php include_once('include/footer.php'); ?>

<?php
    }else {
        header('Location: '.App\Helper::config('config.basePath').'view/admin/login.php');
    }
?>