<?php


include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;

Session::init();
Auth::logout();