<?php

include_once('../../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\user\User;
use App\Order\Order;

Session::init();
$orderObj = new Order();
$userObj = new User();
if(isset($_GET['orderId']) && !empty($_GET['orderId'])) {
    $id             =  filter_var($_GET['orderId'], FILTER_VALIDATE_INT);
    $order          = $orderObj->getOrderById($id);
    $orderProducts  = $orderObj->getOrderProductsByOrderId($id);
    $userDetails    = $userObj->getAllUseInfo($order['customerId']);
   // echo "<pre>";
    //var_dump($orderProducts);

}
?>

<?php include_once('../include/header.php'); ?>

    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Advanced Datatables <small>advanced datatables</small></h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <div class="btn-group btn-theme-panel">
                        <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-settings"></i>
                        </a>
                        <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h3>THEME COLORS</h3>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <ul class="theme-colors">
                                                <li class="theme-color theme-color-default" data-theme="default">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Default</span>
                                                </li>
                                                <li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Blue Hoki</span>
                                                </li>
                                                <li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Blue Steel</span>
                                                </li>
                                                <li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Orange</span>
                                                </li>
                                                <li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Yellow Crusta</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <ul class="theme-colors">
                                                <li class="theme-color theme-color-green-haze" data-theme="green-haze">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Green Haze</span>
                                                </li>
                                                <li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Red Sunglo</span>
                                                </li>
                                                <li class="theme-color theme-color-red-intense" data-theme="red-intense">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Red Intense</span>
                                                </li>
                                                <li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Purple Plum</span>
                                                </li>
                                                <li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Purple Studio</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 seperator">
                                    <h3>LAYOUT</h3>
                                    <ul class="theme-settings">
                                        <li>
                                            Theme Style
                                            <select class="theme-setting theme-setting-style form-control input-sm input-small input-inline tooltips" data-original-title="Change theme style" data-container="body" data-placement="left">
                                                <option value="boxed" selected="selected">Square corners</option>
                                                <option value="rounded">Rounded corners</option>
                                            </select>
                                        </li>
                                        <li>
                                            Layout
                                            <select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
                                                <option value="boxed" selected="selected">Boxed</option>
                                                <option value="fluid">Fluid</option>
                                            </select>
                                        </li>
                                        <li>
                                            Top Menu Style
                                            <select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
                                                <option value="dark" selected="selected">Dark</option>
                                                <option value="light">Light</option>
                                            </select>
                                        </li>
                                        <li>
                                            Top Menu Mode
                                            <select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
                                                <option value="fixed">Fixed</option>
                                                <option value="not-fixed" selected="selected">Not Fixed</option>
                                            </select>
                                        </li>
                                        <li>
                                            Mega Menu Style
                                            <select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
                                                <option value="dark" selected="selected">Dark</option>
                                                <option value="light">Light</option>
                                            </select>
                                        </li>
                                        <li>
                                            Mega Menu Mode
                                            <select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
                                                <option value="fixed" selected="selected">Fixed</option>
                                                <option value="not-fixed">Not Fixed</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                Widget settings form goes here
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn blue">Save changes</button>
                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="#">Home</a><i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="table_advanced.html">Extra</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="table_advanced.html">Data Tables</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li class="active">
                        Advanced Datatables
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-6">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold uppercase">Order Details</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <tbody>
                                        <tr>
                                            <td>Order Id</td>
                                            <td><?php echo $order['orderId']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Items</td>
                                            <td><?php echo $order['userName']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Id</td>
                                            <td><?php echo $order['items']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Amount</td>
                                            <td><?php echo $order['paymentId']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Date</td>
                                            <td><?php echo $order['paymentAmount']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Status</td>
                                            <td><?php echo $order['paymentDate']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Order Status</td>
                                            <td><span class="label label-sm label-info"><?php echo $order['orderStatus']; ?></span></td>
                                        </tr>
                                        <tr>
                                            <td>Order Date</td>
                                            <td><?php echo $order['orderDate']; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                    <div class="col-md-6">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold uppercase">Customer Details</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <tbody>
                                        <tr>
                                            <td>Customer Id</td>
                                            <td><?php echo $userDetails['id']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Customer Name</td>
                                            <td><?php echo $userDetails['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?php echo $userDetails['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Country</td>
                                            <td><?php echo $userDetails['country']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td><?php echo $userDetails['city']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td><?php echo $userDetails['state']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Zip Code</td>
                                            <td><?php echo $userDetails['postal_code']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><?php echo $userDetails['address']; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe font-green-sharp"></i>
                                    <span class="caption-subject font-green-sharp bold uppercase">Order Products Details</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <th>Image</th>
                                            <th>Product ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Subtotal</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if( (count($orderProducts)> 0) ){
                                            foreach ($orderProducts as $product){
                                                ?>
                                                <tr>
                                                    <td class="fit">
                                                        <?php
                                                        $image          = json_decode($product['images']);
                                                        echo '<img style="width: 100px; animation:0s ease 0s normal none 1 running none" src="view/'.$image->small.'" alt=" " class="user-pic">';
                                                        ?>
                                                    </td>
                                                    <td><?php echo $product['id'] ?> </td>
                                                    <td><?php echo $product['name'] ?> </td>
                                                    <td><?php echo '$'.$product['price'] ?></td>
                                                    <td><?php echo $product['sall_qti'] ?></td>
                                                    <td><?php echo $product['total'] ?></td>
                                                </tr>
                                            <?php  } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->


<?php include_once('../include/footer.php'); ?>