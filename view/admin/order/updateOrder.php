<?php

include_once('../../../vendor/autoload.php');

use App\Session;
use App\Auth;
use App\user\User;
use App\Order\Order;

Session::init();
$orderObj = new Order();
if(isset($_GET['action']) && ($_GET['action'] == 'moveToProcess') ){
    if(isset($_GET['orderId'])) {
        //var_dump($_GET['orderId']);
        $orderObj->updateOrderStatus($_GET['orderId'], 'Processing');
        Session::flash('success', 'Order Move To Processing successfully');
        header('location:index.php');
    }
}elseif(isset($_GET['action']) && ($_GET['action'] == 'moveToShipping') ){
    if(isset($_GET['orderId'])) {
        $orderObj->updateOrderStatus($_GET['orderId'], 'Shipping');
        Session::flash('success', 'Order Move To Shipping successfully');
        header('location:index.php');
    }
}elseif(isset($_GET['action']) && ($_GET['action'] == 'moveToSuccess') ){
    if(isset($_GET['orderId'])) {
        $orderObj->updateOrderStatus($_GET['orderId'], 'Success');
        Session::flash('success', 'Order Move To Success successfully');
        header('location:index.php');
    }
}