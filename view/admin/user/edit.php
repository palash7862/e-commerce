<?php
    include_once('../../../vendor/autoload.php');
    use App\Session;
    use App\Auth;
use App\User\User;

    Session::init();
    if(Session::checkSession() == false){
        header("Location:".App\Helper::$basePath);
    }
    include_once('../include/header.php');
    if(count( Auth::getUser()) > 0){
        //$userId = Auth::getUser()['id']; //Auth::getUser() array return kara ar amara array index dara id apaitac
        $result = new User();
        $redults = $result->getAllUseInfo($_GET['id']);
       // $redults = $result->getAllUseInfo($_GET['id']);
       // var_dump($redults);
    }
?>


<div class="container">
    <div class="row">

        <div class="col-md-10 profile" style="margin-top: 25px;">
            <?php if(($redults != false) &&(count($redults)> 0)){ ?>
            <form class="form-horizontal" action="view/admin/user/update.php" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="
    margin-left: 142px;
">Update your profile</legend>

                    <!-- Text input-->




                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Name (Full name)">Name (Full name)</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user">
                                    </i>
                                </div>
                                <input id="Name (Full name)" value="<?= $redults['name'];?>" name="name" type="text" placeholder="Name (Full name)" class="form-control input-md">
                                <input id="id" value="<?= $redults['id'];?>" name="id" type="hidden"  class="form-control input-md">
                            </div>


                        </div>


                    </div>

                    <!-- File Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Upload photo">Upload photo</label>
                        <div class="col-md-4">
                            <input id="Upload photo" name="Upload photo" class="input-file" type="file">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Email Address">Email Address</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope-o"></i>

                                </div>
                                <input id="Email Address" value="<?= $redults['email'];?>" name="email" type="text" placeholder="Email Address" class="form-control input-md">

                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="Password">Password:</label>
                        <div class="col-md-4  col-xs-4">
                            <input id="Password" value="<?= $redults['password'];?>" name="password1" type="text" placeholder="Password" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="State">State:</label>
                        <div class="col-md-4  col-xs-4">
                            <input id="State" value="<?= $redults['state'];?>" name="state" type="text" placeholder="State" class="form-control input-md">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="city">City:</label>
                        <div class="col-md-4  col-xs-4">
                            <input id="city" value="<?= $redults['city'];?>" name="city" type="text" placeholder="city" class="form-control input-md">
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="postcode">Post Code :</label>
                        <div class="col-md-2  col-xs-4">
                            <input id="postcode" value="<?= $redults['postal_code'];?>" name="postal_code" type="text" placeholder="post code" class="form-control input-md ">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Phone">Phone:</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-briefcase"></i>

                                </div>
                                <input id="Phone" value="<?= $redults['phone'];?>" name="phone" type="text" placeholder="Phone" class="form-control input-md">
                            </div>


                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="Address">Address</label>
                        <div class="col-md-4  col-xs-4">
                            <input id="Address" value="<?= $redults['address'];?>" name="address" type="text" placeholder="Address" class="form-control input-md">
                        </div>
                    </div>





                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="Country">Country:</label>
                        <div class="col-md-4  col-xs-4">
                            <input id="Country" value="<?= $redults['country'];?>"  name="country" type="text" placeholder="Country" class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label col-xs-12" for="Country">Role:</label>
                        <div class="col-md-4  col-xs-4">
                            <select name="rule" class="form-control input-md">

                                <option <?php if($redults['rule']=='Admin'){echo 'selected';}?>  >Admin</option>
                                <option <?php if($redults['rule']=='Editor'){echo 'selected';};?> checked>Editor</option>
                                <option <?php if($redults['rule']=='Customer'){echo 'selected';};?> checked>Customer</option>

                            </select>

                        </div>
                    </div>




                    <div class="form-group">
                        <label class="col-md-4 control-label" ></label>
                        <div class="col-md-4">
                            <button type="submit" href="#" class="btn btn-success"><span class="glyphicon glyphicon-thumbs-up"></span> Update</button>
                            <a href="#" class="btn btn-danger" value=""><span class="glyphicon glyphicon-remove-sign"></span> Cencel</a>

                        </div>
                    </div>

                </fieldset>
            </form>
            <?php } ?>
        </div>
        <div class="col-md-2 hidden-xs" style="
    margin-top: 74px;
    margin-left: -52px;">
            <img src="http://websamplenow.com/30/userprofile/images/avatar.jpg" class="img-responsive img-thumbnail ">
        </div>


    </div>
</div>




<?php include_once('../include/footer.php'); ?>
