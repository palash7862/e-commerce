<?php

namespace App\Category;

use PDO;
use PDOException; 
use App\Database;  
use App\Session; 

class Category extends Database
{
    private $table = 'categorys';
    public $name;
    public $parent_id;
    public $description;
    public $id;

    public function getTable(){
        if (!empty($this->table)){
            return  $this->table;
        }
    }

    public function set($data=array()){
        if (array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if (array_key_exists('parent_id',$data)){
            $this->parent_id=$data['parent_id'];
        }
        if (array_key_exists('description',$data)){
            $this->description=$data['description'];
        }
        if (array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        return $this;
    }

    public function store(){
        try {
            $stmt = $this->con->prepare("INSERT INTO `categorys` (`id`, `parent_id`, `name`, `description`) VALUES (NULL, :parent_id, :name, :description);");


            $result = $stmt->execute(array(
                ':parent_id'=>$this->parent_id,
                ':name'=>$this->name,
                ':description'=>$this->description

            ));

            return $result;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    // SHow all cat

    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `categorys` ");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC); 
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getAllCategory(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM {$this->table} ");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
	
	public function getAllParentsCategory(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM {$this->table} WHERE parent_id=:parentId");
            $stmt->bindValue(':parentId', 0, PDO::PARAM_INT);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
		} catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
	
    public function getParentChildCategories($parentId){
        if ($parentId) {
            try {
                $stmt = $this->con->prepare("SELECT * FROM {$this->table} WHERE parent_id=:parentId");
                $stmt->bindValue(':parentId', $parentId, PDO::PARAM_INT);
                $stmt->execute();
                $result = $stmt->fetchAll();
                return $result;
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }
    }

    public function parentCatTree($activeCats = array() ){
        $categoryParents = $this->getAllParentsCategory();
        if ( ($categoryParents != NULL) && (count($categoryParents)>0) ){
            foreach ($categoryParents as $parent){

                echo '<li>';
                    if( in_array($parent['id'], $activeCats) ){
                        echo '<label><input type="checkbox" name="categories[]" value="'.$parent['id'].'" checked>'.$parent['name'].'</label>';
                    }else{
                        echo '<label><input type="checkbox" name="categories[]" value="'.$parent['id'].'">'.$parent['name'].'</label>';
                    }
                    $this->childCatTree($parent['id'], $activeCats);

                echo '</li>';
            }
        }
    }

    public function childCatTree($parentId, $activeCats = array()){
        if ($parentId){
            $childs = $this->getParentChildCategories($parentId);
            if( ($childs != NULL) && (count($childs)>0) ) {
                echo '<ul class="list-unstyled">';
                foreach ($childs as $child) {
                    echo '<li>';
                        if( in_array($child['id'], $activeCats) ){
                            echo '<label><input type="checkbox" name="categories[]" value="'.$child['id'].'" checked>'.$child['name'].'</label>';
                        }else {
                            echo '<label><input type="checkbox" name="categories[]" value="' . $child['id'] . '">' . $child['name'] . '</label>';
                        }
                    echo '</li>';
                    $this->childCatTree($child['id'], $activeCats);
                }
                echo '</ul>';
            }
        }
    }

	// Edit
	
    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `categorys` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                return $stm->fetch(PDO::FETCH_ASSOC);
            }else{

            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `shoping`.`categorys` SET `parent_id` = :parent_id, `name` = :name, `description` = :description WHERE `categorys`.`id` = :id;");
            $stmt->bindValue(':parent_id', $this->parent_id, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':description', $this->description, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);

            $stmt->execute();

            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
	
	public function delete($id){

		try {

			$stm =  $this->con->prepare("DELETE FROM `categorys` WHERE id = :id");
			$stm->bindValue(':id', $id, PDO::PARAM_STR);
			$stm->execute();
			if($stm){
				$_SESSION['delete'] = 'Data successfully Deleted !!!';
				header('location:index.php');
			}

		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
    
	}


}