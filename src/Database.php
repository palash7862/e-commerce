<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 10/2/2017
 * Time: 4:56 PM
 */

namespace App;
use PDO;
use PDOException;



class Database{

    private $dbname  = 'shoping';

    private $dbuser = 'root';

    private $dbpass = '';

    protected $con;

    public function  __construct(){
        try {
            $connection = new PDO('mysql:host=localhost;dbname='.$this->dbname, $this->dbuser, $this->dbpass);
            $this->con = $connection;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    protected function insert($table, $colums, $data){
        try{
            $keys   = implode(', ', $colums);
            $value  = implode(', :', $colums);

            $stmt = $this->con->prepare("INSERT INTO {$table} ({$keys}) VALUES ({$value})");
            foreach ($this->colums as $colum){
                $stmt->bindValue(":{$colum}", $data[$colum], PDO::$validation[$colum]);
            }
            $result = $stmt->execute();
            return $result;
        }catch (PDOException $e){
            return $e->getMessage();
        }
    }



}