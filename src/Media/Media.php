<?php
/**
 * Created by PhpStorm.
 * User: Vergil
 * Date: 10/5/2017
 * Time: 9:00 PM
 */

namespace App\Media;

use PDO;
use App\Database;
use App\Helper;


class Media extends Database{

    protected $table = 'media';

    protected $sizetable = 'media_img_size';

    private $sizeName = null;


    private $sizeWidth = null;

    private $sizeHeight = null;

    private $files = null;

    private $filename = null;
    private $description = null;
    private $location = null;

    public function getTable(){
        if (!empty($this->table)){
            return  $this->table;
        }
    }

    public function setFile($files){
        if (array_key_exists('imageFile',$files)){
            if(count($files['imageFile'])>0) {
                $this->files = $files['imageFile'];
            }
        }
    }

    public function setSize($data){
        if (array_key_exists('name',$data)){
            $this->sizeName = $data['name'];
        }

        if (array_key_exists('width',$data)){
            $this->sizeWidth = $data['width'];
        }

        if (array_key_exists('height',$data)){
            $this->sizeHeight = $data['height'];
        }
    }

    public function getSizeByname($name = null, $rowCount = false){
        if(empty($name)){
            $name = $this->sizeName;
        }
        if(!empty($name)){
            try {
                $stmt = $this->con->prepare("SELECT * FROM {$this->sizetable} WHERE name=:name");

                $stmt->bindValue(":name", $name, PDO::PARAM_STR);
                $stmt->execute();
                if($rowCount === true) {
                    $result = $stmt->rowCount();
                    $result;
                }else{
                    $result = $stmt->fetchAll();
                }
                return $result;
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
    }

    public function getAllSize(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM {$this->sizetable}");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function storeSize(){
        if(($this->sizeName != null) && ($this->sizeWidth != null) && ($this->sizeWidth != null)){
            try {
                $stmt = $this->con->prepare("INSERT INTO {$this->sizetable} ( name, width, height) VALUES ( :name, :width, :height )");

                $stmt->bindValue(":name", $this->sizeName, PDO::PARAM_STR);
                $stmt->bindValue(":width", $this->sizeWidth, PDO::PARAM_INT);
                $stmt->bindValue(":height", $this->sizeHeight, PDO::PARAM_INT);
                $result = $stmt->execute();
                return $result;
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
    }

    public function storeMedia(){
        if(($this->filename != null) && ($this->description != null) && ($this->location != null)){
            try {
                $stmt = $this->con->prepare("INSERT INTO {$this->table} ( name, description, location) VALUES ( :filename, :description, :location )");

                $stmt->bindValue(":filename", $this->filename, PDO::PARAM_STR);
                $stmt->bindValue(":description", $this->description, PDO::PARAM_STR);
                $stmt->bindValue(":location", $this->location, PDO::PARAM_STR);
                $result = $stmt->execute();
                if($result) {
                    $result = $this->con->lastInsertId();
                    return $result;
                }else{
                    return $result;
                }
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
    }

    public function deleteMedia($id){
        try {
            $stmt = $this->con->prepare("DELETE FROM {$this->table} WHERE id=:id");

            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function removeMedia($id){
        if($id) {
            $data = $this->getMediaById($id);
            if($data->location){
                $images = json_decode($data->location);
                $result = unlink('../../'.$images->default);
                $result = unlink('../../'.$images->small);
                if($result){
                    $result = $this->deleteMedia($id);
                    return $result;
                }
            }
        }
    }

    public function getAllMedia(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM {$this->table}");
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getMediaById($id){
        if($id){
            try {
                $stmt = $this->con->prepare("SELECT * FROM {$this->table} WHERE id=:id");

                $stmt->bindValue(":id", $id, PDO::PARAM_INT);
                $stmt->execute();
                $result = $stmt->fetchObject();
                return $result;
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
    }

    public function storeFiles(){
        if ($this->files != null){
            $fileName       = $this->files['name'];
            $fileType       = $this->files['type'];
            $fileTmpName    = $this->files['tmp_name'];
            $fileSize       = $this->files['size'];
            if($this->checkImageFile($fileType, $fileName) === true){
                $upDirectory  = '../../uploads/';
                $makeDir      = $this->makeDirectory($upDirectory);
                $fileOrgName  = $this->makeFilename($fileName, false);
                $newFilename  = $this->makeFilename($fileName);
                if($makeDir != false) {
                    $moveDestination = $makeDir . $newFilename;
                    move_uploaded_file($fileTmpName, $moveDestination);
                    $productImgDestination = $makeDir.$this->makeFilename($fileName, true, 'productImg');
                    $smallDestination = $makeDir.$this->makeFilename($fileName, true, 'smaill');
                    $this->imageResize($moveDestination, $productImgDestination, 380, 250);
                    $this->imageResize($moveDestination, $smallDestination, 60, 60);

                    $defaultDestination = str_replace('../', '', $makeDir).$newFilename;
                    $productImgDestination = str_replace('../', '', $productImgDestination);
                    $smallDestination = str_replace('../', '', $smallDestination);
                    $imagesArray = array(
                        'default'       =>$defaultDestination,
                        'productImg'    =>$productImgDestination,
                        'small'         =>$smallDestination
                    );
                    $this->filename     = $fileOrgName;
                    $this->description  = $fileOrgName;
                    $this->location     = json_encode($imagesArray);

                }
            }
            return false;
        }
        return false;
    }

    public function makeFilename($fileName, $exten = true, $extraname = false){
        if(!empty($fileName)){
            $date = date('Ymdhi');
            $extension = strtolower(end(explode('.', $fileName)));

            $fileOrgName = str_replace('.'.$extension, '', $fileName);
            //$fileOrgName = str_replace(' '.$extension, '', $fileOrgName);
            $fileOrgName = preg_replace('/\s+/', '_', $fileOrgName);
            var_dump($fileOrgName); //preg_replace('', '', $string)
            if($extraname == false){
                $extraname  = '';
            }
            if($exten === false){
                $fileNewName = $fileOrgName;
                return $fileNewName;
            }else {
                $fileNewName = $fileOrgName . '-' . $date.$extraname . '.' . $extension;
                return $fileNewName;
            }
        }
    }

    private function makeDirectory($uploadDir){
        $year = date('Y');
        $month = date('m'); //DIRECTORY_SEPARATOR
        $newDir = $uploadDir.$year.DIRECTORY_SEPARATOR;
        if (is_dir($newDir)) {
            $newDir = $newDir.$month.DIRECTORY_SEPARATOR;
            if(is_dir($newDir)){
                return $newDir;
            }else{
                if(mkdir($newDir, 0777)) {
                    return $newDir;
                }
            }
        }else{
            if(mkdir($newDir, 0777)){
                if(is_dir($newDir)){
                    $newDir = $newDir.$month.DIRECTORY_SEPARATOR;
                    if(mkdir($newDir, 0777)) {
                        return $newDir;
                    }
                }
            }
        }
        return false;
    }

    private function checkImageFile($type, $fileName){
        if( ($type == 'image/jpeg') || ($type == 'image/png') || ($type == 'image/gif') ){
            $extension = strtolower(end(explode('.', $fileName)));
            $typeArray = array('jpg', 'jpeg', 'png', 'gif');
            if(in_array($extension, $typeArray)){
                return true;
            }
            return false;
        }
        return false;
    }

    private function imageResize($newImageNameTest, $location, $thumbNWidth, $thumbNHeight){

        $image = imagecreatefromjpeg( realpath($newImageNameTest) );
        $filename   = $location;
        $thumb_width = $thumbNWidth;
        $thumb_height = $thumbNHeight;
        $width = imagesx($image);
        $height = imagesy($image);
        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;
        if ( $original_aspect >= $thumb_aspect ){
            // If image is wider than thumbnail (in aspect ratio sense)
            $new_height = $thumb_height;
            $new_width = $width / ($height / $thumb_height);
        }else{
            // If the thumbnail is wider than the image
            $new_width = $thumb_width;
            $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

        // Resize and crop
        imagecopyresampled($thumb,
            $image,
            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
            0, 0,
            $new_width, $new_height,
            $width, $height);
        imagejpeg($thumb, $filename, 100);
        return true;
    }

}