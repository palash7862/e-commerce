<?php
/**
 * Created by PhpStorm.
 * User: Vergil
 * Date: 10/10/2017
 * Time: 3:53 AM
 */

namespace App\Cart;

use App\Auth;
use App\Session;
use App\Product\Product;

class Cart extends Session{

    public function addProduct($productId, $qti = 1){
        $data = array();
        $data = parent::get('cart');
        $result = $this->createProductArray($productId, $qti);
        if ($result != false) {
            $data[$productId] = $result;
            $result = parent::set('cart', $data);
            $cart = parent::set('CPSC', 50);
            if( ($result === true) && ($cart === true) ){
                return true;
            }
            return false;
        }
        return false;
    }

    public function getCartAll(){
        $result = parent::get('cart');
        if($result != false){
            return $result;
        }
        return false;
    }

    private function createProductArray($productId, $qti){
        $productObj = new Product();
        $result = $productObj->getProductById($productId);
        if ( ($result != null) && (count($result)>0) ){
            $data = array();
            $data['id']                 = $result['id'];
            $data['name']               = $result['name'];
            $data['short_description']  = $result['short_description'];
            $data['price']              = $result['price'];
            $data['qti']                = $qti;
            $data['Subtotal']           = (int) $qti * (int) $result['price'];
            $image                      = json_decode( explode('; ', $result['images'] )[0] );
            $data['image']              = $image->small;
            return $data;
        }
        return false;
    }

    public function updateCartProductById($id, $newData){
        $oldData = parent::get('cart');
        if($oldData != false){
            if(array_key_exists($id, $oldData)){
                foreach ($newData as $key => $value){
                    if( array_key_exists( $key, $oldData[$id]) ){
                        $oldData[$id][$key] = $value;
                        if( $key == 'qti'){
                            $oldData[$id]['Subtotal'] = $oldData[$id]['price'] * $value;
                        }
                        if( $key == 'price' ){
                            $oldData[$id]['Subtotal'] = $oldData[$id]['qti'] * $value;
                        }
                    }
                }
                $result =  parent::set('cart', $oldData);
                if ($result === true){
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public function removeProduct($productId){
        $data = parent::get('cart');
        if ( (count($data)>0) && array_key_exists($productId, $data) ) {
            unset($data[$productId]);
            $result =  parent::set('cart', $data);
            //var_dump($result);
            if ($result === true){
                return true;
            }
            return false;
        }
        return false;
    }

    public function cartItem(){
        $data = parent::get('cart');
        if (is_array($data)) {
            return count($data);
        }
        return false;
    }

    public function getTotalPrice(){
        $data = $this->getCartAll();
        $price = 0;
        if (is_array($data) && (count($data) > 0) ) {
            foreach ($data as $product){
                $price += (int) $product['Subtotal'];
            }
            return $price;
        }
        return $price;
    }

    public function cartClear(){
        parent::keyUnSet('cart');
        return true;
    }

}