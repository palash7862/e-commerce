<?php
/**
 * Created by PhpStorm.
 * User: Vergil
 * Date: 10/12/2017
 * Time: 4:36 AM
 */

namespace App\Order;
use PDO;
use PDOException;
use App\Database;
use App\Session;
use App\Auth;
use App\user\User;
use App\Cart\Cart;

class Order extends Database {


    private $table = 'orders';

    private $tableDetails = 'ordersdetails';

    protected $user = null;

    protected $userTable = null;

    private $customerId;
    private $shipDate = null;
    private $paymentId;
    private $paymentAmount;
    private $paymentDate;
    private $paymentStatus;
    private $orderStatus;
    private $orderProducts;

    public function __construct(){
        parent::__construct();
        $this->user         = new User();
        $this->userTable    = $this->user->getTable();
    }

    public function set($customerId, $orderData, $products){
        if(!empty($customerId)){
            $this->customerId = $customerId;
        }

        if(array_key_exists('paymentId', $orderData)){
            $this->paymentId = $orderData['paymentId'];
        }

        if(array_key_exists('paymentAmount', $orderData)){
            $this->paymentAmount = $orderData['paymentAmount'];
        }

        if(array_key_exists('paymentDate', $orderData)){
            $this->paymentDate = $orderData['paymentDate'];
        }

        if(array_key_exists('paymentStatus', $orderData)){
            $this->paymentStatus = $orderData['paymentStatus'];
        }

        if(array_key_exists('orderStatus', $orderData)){
            $this->orderStatus = $orderData['orderStatus'];
        }

        if(count($products)>0){
            $this->orderProducts = $products;
        }

    }

    public function insertOrder(){
        try {
            $stmt = $this->con->prepare("INSERT INTO $this->table (customerId, shipDate, paymentId, paymentAmount, paymentDate, paymentStatus, orderStatus) VALUES (:customerId, :shipDate, :paymentId, :paymentAmount, :paymentDate, :paymentStatus, :orderStatus)");

            $stmt->bindValue(":customerId", $this->customerId, PDO::PARAM_INT);
            $stmt->bindValue(":shipDate", $this->shipDate, PDO::PARAM_STR);
            $stmt->bindValue(":paymentId", $this->paymentId, PDO::PARAM_INT);
            $stmt->bindValue(":paymentAmount", $this->paymentAmount, PDO::PARAM_STR);
            $stmt->bindValue(":paymentDate", $this->paymentDate, PDO::PARAM_STR);
            $stmt->bindValue(":paymentStatus", $this->paymentStatus, PDO::PARAM_STR);
            $stmt->bindValue(":orderStatus", $this->orderStatus, PDO::PARAM_STR);
            $result = $stmt->execute();
            $orderId = $this->con->lastInsertId();
            $this->orderProductInsert($orderId);
            if($result){
                header('location: view/front/index.php');
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function updateOrderStatus($id, $status){
        try {

            $stmt = $this->con->prepare("UPDATE {$this->table} SET orderStatus = :status WHERE orderId = :id");
            $stmt->bindValue(':status', $status, PDO::PARAM_STR);
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $result = $stmt->execute();

            if($result){
                return $result;
            }
            return false;

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function orderProductInsert($orderId){
        foreach ($this->orderProducts as $product){
            $product['code'] = 'sjhdkfhs';
            //var_dump($product);
            $stm =  $this->con->prepare("INSERT INTO {$this->tableDetails} ( orderId, productId, productcode, quantity, total ) VALUES (:orderId, :productId, :productcode, :quantity, :total)");
            $stm->bindValue(':orderId', $orderId);
            $stm->bindValue(':productId', $product['id']);

            $stm->bindValue(':productcode', $product['code']);
            $stm->bindValue(':quantity', $product['qti']);
            $stm->bindValue(':total', $product['price']);
            $result = $stm->execute();
            return $result;
            //var_dump($product['qti']);
        }

    }

    public function getOrders($status = false){
        try{
            $query = "SELECT {$this->table}.*, users.id AS userId, users.name AS userName, (SELECT COUNT({$this->tableDetails}.id ) FROM {$this->tableDetails} WHERE {$this->tableDetails}.orderId = {$this->table}.orderId  ) AS items ";
            $query .= "FROM {$this->table} ";
            $query .= "LEFT JOIN {$this->userTable} on {$this->userTable}.id = {$this->table}.customerId ";
            $query .= "LEFT JOIN {$this->tableDetails} on {$this->tableDetails}.orderId = {$this->table}.orderId ";

            if($status != false) {
                $query .= "WHERE {$this->table}.orderStatus=:ending ";
            }
            $query .= "GROUP BY {$this->table}.orderId";
            $stmt = $this->con->prepare($query);
            if($status != false) {
                $stmt->bindValue(":ending", $status, PDO::PARAM_STR);
            }

            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getOrderById($id){
        try{
            $query = "SELECT {$this->table}.*, users.id AS userId, users.name AS userName, (SELECT COUNT({$this->tableDetails}.id ) FROM {$this->tableDetails} WHERE {$this->tableDetails}.orderId = {$this->table}.orderId  ) AS items ";
            $query .= "FROM {$this->table} ";
            $query .= "LEFT JOIN {$this->userTable} on {$this->userTable}.id = {$this->table}.customerId ";
            $query .= "LEFT JOIN {$this->tableDetails} on {$this->tableDetails}.orderId = {$this->table}.orderId ";
            $query .= "WHERE {$this->table}.orderId=:id ";
            $query .= "GROUP BY {$this->table}.orderId";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);

            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function getOrderProductsByOrderId($id){
        try{
            $query = "SELECT ordersdetails.id AS odId, ordersdetails.quantity AS sall_qti,ordersdetails.total,  products.*, ( SELECT GROUP_CONCAT(media.location SEPARATOR  '; ') FROM media WHERE media.id=product_images.media_id ) AS images FROM  ordersdetails  ";
            $query .= "LEFT JOIN products on products.id = ordersdetails.productId ";
            $query .= "LEFT JOIN product_images on product_images.product_id = products.id ";
            $query .= "LEFT JOIN media on media.id = product_images.media_id ";
            $query .= "WHERE ordersdetails.orderId=25 GROUP BY ordersdetails.productId ";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);

            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public static function stap(){
        if($_SERVER['REQUEST_METHOD'] == 'GET') {
            $getRequest = $_GET;
            if (isset($getRequest['stap'])) {
                if ($getRequest['stap'] == 2) { //2
                    $cartObj = new Cart();
                    if ($cartObj->cartItem() > 0) {
                        if (Session::checkSession() == true) {
                            return 3;
                        }
                        return 2;
                    }
                    return 1;
                }
            }
            return 1;
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            $getRequest = $_POST;
            if (isset($getRequest['stap']) && ( $getRequest['stap'] == 3 )){
                return 4;
            }
            return 2;
        }
    }

}