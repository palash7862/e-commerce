<?php
/**
 * Created by PhpStorm.
 * User: Vergil
 * Date: 10/3/2017
 * Time: 12:15 AM
 */
namespace App\User;

use App\Database;
use PDO;

class User extends Database {

    protected $table = 'users';

    protected $colums = array('name', 'email', 'password', 'state', 'city', 'postal_code', 'phone', 'address', 'country', 'rule', 'activation_key', 'user_status');

    private $sql      = null;

    public function getTable(){
        if(!empty($this->table)){
            return $this->table;
        }
    }

    public function create($data){
//            $keys   = implode(', ', $this->colums);
//            $value  = implode(', :', $this->colums);

            $stmt = $this->con->prepare("INSERT INTO $this->table (name, email, password, state, city, postal_code, phone, address, country, rule, activation_key, user_status) VALUES (:name, :email, :password, :state, :city, :postal_code, :phone, :address, :country, :rule, :activation_key, :user_status)");

            $stmt->bindValue(":name", $data['name'], PDO::PARAM_STR);
            $stmt->bindValue(":email", $data['email'], PDO::PARAM_STR);
            $stmt->bindValue(":password", md5($data['password']), PDO::PARAM_STR);
            $stmt->bindValue(":state", $data['state'], PDO::PARAM_STR);
            $stmt->bindValue(":city", $data['city'], PDO::PARAM_STR);
            $stmt->bindValue(":postal_code", $data['postal_code'], PDO::PARAM_STR);
            $stmt->bindValue(":phone", $data['phone'], PDO::PARAM_STR);
            $stmt->bindValue(":address", $data['address'], PDO::PARAM_STR);
            $stmt->bindValue(":country", $data['country'], PDO::PARAM_STR);
            $stmt->bindValue(":rule", 'Customer', PDO::PARAM_STR);
            $stmt->bindValue(":activation_key", rand(3333,1000));
            $stmt->bindValue(":user_status", '1', PDO::PARAM_STR);
            $result = $stmt->execute();
            return $result;
    }

    public function getUserByNameOrEmail($email, $pass){
        $stmt = $this->con->prepare("SELECT * FROM $this->table WHERE (name=:email OR email=:email) AND password=:pass");
        $stmt->bindValue(":email", $email, PDO::PARAM_STR);
        $stmt->bindValue(":pass", $pass, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchObject();
        return $result;
    }

    public function getAllUseInfo($id){

        $stmt = $this->con->prepare("SELECT * FROM $this->table WHERE id=:id");
        $stmt->bindValue(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;

    }

    public function deleteUser($id){
            try {
                $stmt = $this->con->prepare("DELETE FROM $this->table WHERE id=:id");
                $stmt->bindValue(":id", $id, PDO::PARAM_STR);
                $stmt->execute();

                if ($stmt) {
                    header('location: index.php');
                }
            } catch (PDOException $e) {
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
        }






    public function update($data){
        try {
            $stmt = $this->con->prepare("UPDATE $this->table SET `name` = :name, `email` = :email, `country` = :country, `city` = :city, `state` = :state, `postal_code` = :postal_code, `phone` = :phone, `address` = :address, `rule` = :rule WHERE `users`.`id` = :id;"); //update table name
            $stmt->bindValue(':name', $data['name'], PDO::PARAM_INT);
            $stmt->bindValue(':email', $data['email'], PDO::PARAM_INT);

            $stmt->bindValue(':city', $data['city'], PDO::PARAM_INT);
            $stmt->bindValue(':state', $data['state'], PDO::PARAM_INT);
            $stmt->bindValue(':postal_code', $data['postal_code'], PDO::PARAM_INT);
            $stmt->bindValue(':phone', $data['phone'], PDO::PARAM_INT);
            $stmt->bindValue(':address', $data['address'], PDO::PARAM_INT);
            $stmt->bindValue(':rule', $data['rule'], PDO::PARAM_INT);
            $stmt->bindValue(':country', $data['country'], PDO::PARAM_INT);
            $stmt->bindValue(':id', $data['id'], PDO::PARAM_INT);
            $stmt->execute();

            if($stmt && !empty($data['rule'])):

                header('location: ../user/index.php');
            elseif ($stmt):
                header('location: profile.php');
            else: header('location: index.php');
            endif;
         //return $stmt;

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function passUpdate($data){
        try {
            $stmt = $this->con->prepare("UPDATE $this->table SET `password` = :password WHERE `users`.`id` = :id;"); //update table name
            $stmt->bindValue(':password', md5($data['password1']), PDO::PARAM_INT);
            $stmt->bindValue(':id', $data['id'], PDO::PARAM_INT);
            $stmts = $stmt->execute();
            //var_dump($stmt);
           // die();

         return $stmts;

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }





    public function getAllUsersInfo(){

        $stmt = $this->con->prepare("SELECT * FROM $this->table");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            return $result;
        }
        return false;

    }



}