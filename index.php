<?php


include_once('vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Helper;
use App\Product\Product;
use App\Cart\Cart;

Session::init();
$productObj  = new Product();
$cartObj     = new Cart();
$allProducts = $productObj->getAllProducts();

$recentProducts = $productObj->getAllProducts(4, 'DESC');
$cartData    = $cartObj->getCartAll();
$subtotal    = $cartObj->getTotalPrice();

//var_dump( Session::get('user') );
//var_dump($_GET['url']);
if( isset($_GET['url']) && ( ($_GET['url'] == 'admin') || ($_GET['url'] == 'admin/') ) ){
    if (Auth::checkRule('Admin') || Auth::checkRule('Editor')){
        header('Location: '.App\Helper::config('config.basePath').'view/admin/index.php');
    }else{
        header('Location: '.App\Helper::config('config.basePath').'view/admin/login.php');
    }
}else{
    include_once('view/front/index.php');
}

?>