-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2017 at 08:08 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shoping`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE IF NOT EXISTS `categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(2, 2, 'Woman', '<li class=" dropdown-submenu">\r\n                                <a href="view/admin/product/view.php">Manage Porduct</a>\r\n                            </li>', '2017-10-04 10:08:18', '2017-10-04 10:08:18'),
(72, 2, 'ff', 'ff', '2017-10-08 20:23:09', '2017-10-08 20:23:09'),
(73, 2, 'sssssssssss', '', '2017-10-12 10:58:13', '2017-10-12 10:58:13'),
(74, 73, 'fffffffffffff', 'fffffffff', '2017-10-12 10:58:20', '2017-10-12 10:58:20'),
(75, 74, 'ffffff', '', '2017-10-12 11:00:01', '2017-10-12 11:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `location` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `description`, `location`, `created_at`, `updated_at`) VALUES
(1, 'laptop', NULL, '', '2017-10-04 10:25:43', '2017-10-04 10:25:43'),
(2, 'problem', 'problem', '{"default":"uploads\\/2017\\\\10\\\\problem-201710080716.png","small":"uploads\\/2017\\\\10\\\\problem-201710080716smaill.png"}', '2017-10-08 17:16:07', '2017-10-08 17:16:07'),
(3, 'problem2', 'problem2', '{"default":"uploads\\/2017\\\\10\\\\problem2-201710080717.png","small":"uploads\\/2017\\\\10\\\\problem2-201710080717smaill.png"}', '2017-10-08 17:17:35', '2017-10-08 17:17:35'),
(4, '55Laney69_Flower Rains_akdjSQ-201710090134', '55Laney69_Flower Rains_akdjSQ-201710090134', '{"default":"uploads\\/2017\\\\10\\\\55Laney69_Flower Rains_akdjSQ-201710090134-201710090918.jpg","small":"uploads\\/2017\\\\10\\\\55Laney69_Flower Rains_akdjSQ-201710090134-201710090918smaill.jpg"}', '2017-10-09 19:18:20', '2017-10-09 19:18:20'),
(5, 'cat_crea', 'cat_crea', '{"default":"uploads\\/2017\\\\10\\\\cat_crea-201710100716.png","small":"uploads\\/2017\\\\10\\\\cat_crea-201710100716smaill.png"}', '2017-10-10 17:16:48', '2017-10-10 17:16:48'),
(6, 'cat_man', 'cat_man', '{"default":"uploads\\/2017\\\\10\\\\cat_man-201710100716.png","small":"uploads\\/2017\\\\10\\\\cat_man-201710100716smaill.png"}', '2017-10-10 17:16:50', '2017-10-10 17:16:50'),
(7, 'dashboard', 'dashboard', '{"default":"uploads\\/2017\\\\10\\\\dashboard-201710100716.png","small":"uploads\\/2017\\\\10\\\\dashboard-201710100716smaill.png"}', '2017-10-10 17:16:53', '2017-10-10 17:16:53'),
(11, 'cat_crea', 'cat_crea', '{"default":"uploads\\/2017\\\\10\\\\cat_crea-201710120739.png","productImg":"uploads\\/2017\\\\10\\\\cat_crea-201710120739productImg.png","small":"uploads\\/2017\\\\10\\\\cat_crea-201710120739smaill.png"}', '2017-10-12 17:39:39', '2017-10-12 17:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `short_description` mediumtext NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `status` enum('Published','Not Published') NOT NULL,
  `created_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `code`, `name`, `description`, `short_description`, `quantity`, `price`, `status`, `created_it`, `updated_it`) VALUES
(1, 0, '', 'name', 'description', 'short description', 1, '1212', 'Published', '2017-10-05 20:49:49', '2017-10-05 20:49:49'),
(3, 0, '', 'Shahin', 'he is  a young man', 'no comments', 10, '40', 'Published', '2017-10-05 21:08:43', '2017-10-05 21:08:43'),
(4, 0, '', 'dddddddddp', 'dddddddddp', 'ddddddddddddddp', 4, '4', 'Not Published', '2017-10-05 21:12:44', '2017-10-05 21:12:44'),
(41, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:44:48', '2017-10-08 19:44:48'),
(42, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:45:58', '2017-10-08 19:45:58'),
(43, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:46:19', '2017-10-08 19:46:19'),
(44, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:46:22', '2017-10-08 19:46:22'),
(45, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:46:23', '2017-10-08 19:46:23'),
(46, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:47:26', '2017-10-08 19:47:26'),
(47, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:47:36', '2017-10-08 19:47:36'),
(48, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:48:45', '2017-10-08 19:48:45'),
(49, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:48:50', '2017-10-08 19:48:50'),
(50, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:50:05', '2017-10-08 19:50:05'),
(51, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:50:07', '2017-10-08 19:50:07'),
(52, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:50:08', '2017-10-08 19:50:08'),
(53, 0, '', '', '', '', 0, '0', '', '2017-10-08 19:50:09', '2017-10-08 19:50:09'),
(54, 0, '', '', '', '', 0, '0', '', '2017-10-08 20:20:40', '2017-10-08 20:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `product_categorys`
--

CREATE TABLE IF NOT EXISTS `product_categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `rule` enum('Admin','Editor','Customer','') NOT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `user_status` enum('-1','0','1','') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `country`, `city`, `state`, `postal_code`, `phone`, `address`, `image`, `rule`, `activation_key`, `user_status`, `created_at`, `updated_at`) VALUES
(11, 'admin', 'admin@gmail.com', '202cb962ac59075b964b07152d234b70', 'Bangladeshi', 'Dhakaa', 'Dhaka', '2456', '1926518051000', 'admin Dhaka, Adabor, bangladesh', '', 'Admin', '3199', '1', '2017-10-04 10:01:39', '2017-10-04 10:01:39'),
(12, 'Md . shahinur Islam', 'shahin6644@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', '', 'Customer', '1710', '1', '2017-10-12 04:52:59', '2017-10-12 04:52:59'),
(13, 'shishir hhh', 'shiuuuu@mail.com', '2b24d495052a8ce66358eb576b8912c8', '', '', '', '', '', '', '', 'Customer', '1536', '1', '2017-10-12 10:16:25', '2017-10-12 10:16:25'),
(14, 'shahin', 'shahin@mail.xom', 'a1b0d5228df73c24196f32b30acc5285', 'BAngladesh', 'Dhaka', 'Mohammadpur', '2456', '01926518051', 'Dhaka, Adabor, banglades', '', 'Customer', '2850', '1', '2017-10-12 10:44:04', '2017-10-12 10:44:04'),
(15, 'kashamh', 'kasham@gmail.comh', '202cb962ac59075b964b07152d234b70', 'hhhhhhhhh', 'hhhhhhhhhhh', 'hhhhhhhhhhhhhh', '55', '555555555', 'hhhhhhhhhh', '', 'Customer', NULL, '1', '2017-10-12 18:00:00', '2017-10-12 18:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
