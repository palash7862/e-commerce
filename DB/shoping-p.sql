-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2017 at 07:30 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoping`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 0, 'Man', NULL, '2017-10-04 10:08:18', '2017-10-04 10:08:18'),
(2, 0, 'Woman', NULL, '2017-10-04 10:08:18', '2017-10-04 10:08:18'),
(3, 1, 'T-Shart', NULL, '2017-10-04 10:09:21', '2017-10-04 10:09:21'),
(4, 2, 'Three pice', NULL, '2017-10-04 10:09:21', '2017-10-04 10:09:21'),
(6, 3, 'Three', NULL, '2017-10-08 19:27:15', '2017-10-08 19:27:15'),
(7, 6, 'four 1', NULL, '2017-10-08 19:30:25', '2017-10-08 19:30:25'),
(8, 6, 'Four 2', NULL, '2017-10-08 19:30:25', '2017-10-08 19:30:25'),
(9, 1, 'dfgd', NULL, '2017-10-08 19:37:02', '2017-10-08 19:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `location` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `description`, `location`, `created_at`, `updated_at`) VALUES
(21, 'me_nicoll_Pencil_ZUFjQg', 'me_nicoll_Pencil_ZUFjQg', '{\"default\":\"uploads\\/2017\\\\10\\\\me_nicoll_Pencil_ZUFjQg-201710140106.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\me_nicoll_Pencil_ZUFjQg-201710140106productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\me_nicoll_Pencil_ZUFjQg-201710140106smaill.jpg\"}', '2017-10-13 23:06:30', '2017-10-13 23:06:30'),
(22, '26233-pirates-of-the-caribbean-on-stranger-tides-1920x1200-movie-wallpaper', '26233-pirates-of-the-caribbean-on-stranger-tides-1920x1200-movie-wallpaper', '{\"default\":\"uploads\\/2017\\\\10\\\\26233-pirates-of-the-caribbean-on-stranger-tides-1920x1200-movie-wallpaper-201710140106.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\26233-pirates-of-the-caribbean-on-stranger-tides-1920x1200-movie-wallpaper-201710140106productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\26233-pirates-of-the-caribbean-on-stranger-tides-1920x1200-movie-wallpaper-201710140106smaill.jpg\"}', '2017-10-13 23:06:56', '2017-10-13 23:06:56'),
(23, '547986', '547986', '{\"default\":\"uploads\\/2017\\\\10\\\\547986-201710140107.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\547986-201710140107productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\547986-201710140107smaill.jpg\"}', '2017-10-13 23:07:19', '2017-10-13 23:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `media_img_size`
--

CREATE TABLE `media_img_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_img_size`
--

INSERT INTO `media_img_size` (`id`, `name`, `width`, `height`) VALUES
(1, 'palash', 150, 222),
(2, 'dfdfg', 654, 4545),
(3, 'cvbbvc', 4544, 4865),
(11, 'ghfghf', 45645, 46546),
(12, 'jkl', 45645, 4564),
(13, 'gfhfgh', 45555564, 645654),
(14, 'cvbbvcfgfghghf', 414, 4564),
(15, 'ghfhg', 4454, 46564),
(16, 'fghfgghf', 445, 46546);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `shipDate` date DEFAULT NULL,
  `paymentId` varchar(255) NOT NULL,
  `paymentAmount` decimal(10,0) NOT NULL,
  `paymentDate` datetime NOT NULL,
  `paymentStatus` enum('Accept','Not Accept') NOT NULL,
  `orderStatus` enum('Pending','Processing','Shipping','Success') NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderId`, `customerId`, `shipDate`, `paymentId`, `paymentAmount`, `paymentDate`, `paymentStatus`, `orderStatus`, `orderDate`) VALUES
(29, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:04:46'),
(30, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:19:18'),
(31, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:20:53'),
(32, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:20:56'),
(33, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:21:01'),
(34, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:22:01'),
(35, 11, NULL, '4444564', '649', '2017-10-13 00:00:00', 'Accept', 'Pending', '2017-10-16 05:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `ordersdetails`
--

CREATE TABLE `ordersdetails` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productcode` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordersdetails`
--

INSERT INTO `ordersdetails` (`id`, `orderId`, `productId`, `productcode`, `quantity`, `total`) VALUES
(12, 29, 19, 'sjhdkfhs', 1, '599'),
(13, 30, 19, 'sjhdkfhs', 1, '599'),
(14, 31, 19, 'sjhdkfhs', 1, '599'),
(15, 32, 19, 'sjhdkfhs', 1, '599'),
(16, 33, 19, 'sjhdkfhs', 1, '599'),
(17, 34, 19, 'sjhdkfhs', 1, '599'),
(18, 35, 19, 'sjhdkfhs', 1, '599');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` mediumtext NOT NULL,
  `description` mediumtext,
  `price` decimal(10,0) NOT NULL,
  `status` enum('Published','Not Published') NOT NULL,
  `created_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `code`, `name`, `short_description`, `description`, `price`, `status`, `created_it`, `updated_it`, `quantity`) VALUES
(19, 0, '', 'T shirt ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nWhy do we use it?\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', '599', 'Published', '2017-10-10 10:56:38', '2017-10-10 10:56:38', 66),
(20, 0, '', 'Shire', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nWhy do we use it?\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', '1500', 'Published', '2017-10-10 10:57:38', '2017-10-10 10:57:38', 200),
(21, 0, '', 'game', 'dfffffffgggggggggggggggggggggggggggggg', 'dsfffffffffffffffff', '152', 'Published', '2017-10-13 20:15:55', '2017-10-13 20:15:55', 4556);

-- --------------------------------------------------------

--
-- Table structure for table `product_categorys`
--

CREATE TABLE `product_categorys` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_categorys`
--

INSERT INTO `product_categorys` (`id`, `product_id`, `category_id`) VALUES
(52, 18, 1),
(53, 18, 2),
(66, 19, 1),
(67, 19, 3),
(68, 20, 2),
(72, 21, 1),
(73, 21, 6),
(74, 21, 7),
(75, 21, 8);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `media_id`) VALUES
(32, 18, 8),
(33, 18, 9),
(44, 19, 21),
(45, 20, 22),
(47, 21, 23);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `rule` enum('Admin','Editor','Customer','') NOT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `user_status` enum('-1','0','1','') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `country`, `city`, `state`, `postal_code`, `phone`, `address`, `image`, `password`, `rule`, `activation_key`, `user_status`, `created_at`, `updated_at`) VALUES
(11, 'admin', 'admin@gmail.com', 'Bangladesh', 'Dhaka', 'Dhaka', '1213', '098764564513', 'dhaka m sola', '', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', '3199', '1', '2017-10-04 10:01:39', '2017-10-04 10:01:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_img_size`
--
ALTER TABLE `media_img_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `ordersdetails`
--
ALTER TABLE `ordersdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categorys`
--
ALTER TABLE `product_categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `media_img_size`
--
ALTER TABLE `media_img_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `ordersdetails`
--
ALTER TABLE `ordersdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `product_categorys`
--
ALTER TABLE `product_categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
